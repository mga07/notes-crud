import * as express from 'express';
import { notesRoute } from './src/modules/notes/notes.routes';
export class Routes {
    protected app: express.Application;
    constructor() {

    }

    public routePath() {
        const router: express.Router = express.Router();
        router.use('/notes', notesRoute );
        router.all('/*', (req, res) => {
            res.send('Welcome to Notes CRUD project :) ');
        });
        return router;
    }
}
