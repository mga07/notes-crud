import { log } from "./../../logger";
import * as dotenv from "dotenv";
import * as mongoose from "mongoose";

dotenv.config();
export class DatabaseInit {
  // connection for database
  private log = log.getLogger();
  constructor() {
    this.connectDatabase();
  }

  private async connectDatabase() {
    try {
      const url = this.getConnectURL();
      mongoose.connect(
        url,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        },
        (err) => {
          if (err) {
            this.log.info(`Unable to connect DB ${err}`);
            throw err;
          }
          this.log.info(`Connected to the Database successfully`);
        }
      );
    } catch (err) {
      this.log.error(`mongo Connection error ---- `, err);
    }
  }

  private getConnectURL() {
    const url = process.env.MONGO_URL;
    return url;
  }
}
