import { Schema, model as Model } from "mongoose";
import { Tables } from "../config/tables";


const notesSchema: Schema = new Schema({
    title: {
        type: String,
        required: true
    },
    // content is optional
    content: {
        type: String
    }
}, { timestamps: true })

const notes = Model(Tables.notes, notesSchema);
export default notes;