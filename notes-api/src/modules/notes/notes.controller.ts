import { Mongoose } from "mongoose";
import { log } from "../../../logger";
import { CONSTANCE } from "../../config/constance";
import { ResponseBuilder } from "../../helpers/responseBuilder";
import { NotesService } from "./notes.service";

export class Notes {
    public logger = log.getLogger();
    public notesService = new NotesService();
    public getNotes = async (req, res) => {
        try {
            const result: ResponseBuilder = await this.notesService.getNotes()

            // { code: 200, msg: '', description: '', error: '', result: [] };
            return res.status(result.code).send({ status: CONSTANCE.SUCCESS, data: result.result });

        } catch (err) {
            this.logger.log(`get notes Error  ---`, err);
            throw ResponseBuilder.error(err);

        }
    }

    public getById = async (req, res) => {
        try {
            const { id } = req.params;
            const mongo = new Mongoose();
            if (id && mongo.isValidObjectId(id)) {
                const result: ResponseBuilder = await this.notesService.getById(id)
                if (result && result.code === CONSTANCE.RES_CODE.success) {
                    return res.status(result.code).send({ status: CONSTANCE.SUCCESS, data: result.result });
                } else {
                    return res.status(result.code).send({ status: CONSTANCE.FAIL, data: result.error });
                }
            } else {
                const result = ResponseBuilder.badRequest('invalid id');
                return res.status(result.code).send({ status: CONSTANCE.BAD_DATA, message: result.error });
            }

        } catch (err) {
            this.logger.log(`get by id Error  ---`, err);
            throw ResponseBuilder.error(err);

        }
    }

    public editNote = async (req, res) => {
        try {
            const { id } = req.params;
            const mongo = new Mongoose();
            if (id && mongo.isValidObjectId(id)) {
                const result: ResponseBuilder = await this.notesService.editNote(id, req.body)
                if (result && result.code === CONSTANCE.RES_CODE.success) {
                    return res.status(result.code).send({ status: CONSTANCE.SUCCESS, data: result.result, msg: 'Note updated successfully' });
                } else {
                    return res.status(result.code).send({ status: CONSTANCE.FAIL, data: result.error });
                }
            } else {
                const result = ResponseBuilder.badRequest('invalid id');
                return res.status(result.code).send({ status: CONSTANCE.BAD_DATA, message: result.error });
            }

        } catch (err) {
            this.logger.log(`edit note Error  ---`, err);
            throw ResponseBuilder.error(err);

        }
    }

    public createNote = async (req, res) => {
        try {
            const result: ResponseBuilder = await this.notesService.createNote(req.body)
            return res.status(result.code).send({ status: CONSTANCE.SUCCESS, data: result.result, msg: 'Note created successfully' });
        } catch (err) {
            this.logger.log(`create note Error  ---`, err);
            throw ResponseBuilder.error(err);

        }
    }

    public deleteNote = async (req, res) => {
        try {
            const { id } = req.params;
            const mongo = new Mongoose();
            if (id && mongo.isValidObjectId(id)) {
                const result: ResponseBuilder = await this.notesService.deleteNote(id)
                return res.status(result.code).send({ status: CONSTANCE.SUCCESS, data: result.msg });
            } else {
                const result = ResponseBuilder.badRequest('invalid id');
                return res.status(result.code).send({ status: CONSTANCE.BAD_DATA, message: result.error });
            }

        } catch (err) {
            this.logger.log(`delete note Error  ---`, err);
            throw ResponseBuilder.error(err);

        }
    }
}