import { IsNotEmpty, IsString } from "class-validator";
import { model } from "../../model";

export class AddNote extends model {
  @IsNotEmpty()
  @IsString()
  public title: string;

  @IsString()
  public content: string;

  constructor(body) {
    super();
    const { title, content } = body;
    this.title = title;
    this.content = content;
  }
}

export class EditNote extends AddNote {}
