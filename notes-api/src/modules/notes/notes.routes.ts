import { Router } from 'express';
import { Notes } from './notes.controller';
import { validator } from '../../validate';
import { AddNote, EditNote } from './notes.modal';

const router: Router = Router();
const notesController = new Notes();
const V: validator = new validator();

router.post('/', V.validate(AddNote), notesController.createNote)
router.get('/', notesController.getNotes);
router.get('/:id', notesController.getById);
router.put('/:id', V.validate(EditNote), notesController.editNote)
router.delete('/:id', notesController.deleteNote);

export const notesRoute: Router = router;