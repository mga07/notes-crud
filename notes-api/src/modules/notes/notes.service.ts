import { Mongoose, mongo } from "mongoose";
import { log } from "../../../logger";
import { ResponseBuilder } from "../../helpers/responseBuilder";
import notes from "../../model/notes.schema";
import { AddNote, EditNote } from "./notes.modal";

export class NotesService {
    public log = log.getLogger();

    public getNotes = async (): Promise<ResponseBuilder> => {
        try {
            const fetchedNotes = await notes.find({}).sort({'createdAt': -1});
            return ResponseBuilder.data(fetchedNotes, 'Notes fetched successfully');
        } catch (err) {
            throw ResponseBuilder.error(err);
        }
    }

    public getById = async (id: string): Promise<ResponseBuilder> => {
        try {
            const fetchedNoteById = await notes.findById(id);
            if(fetchedNoteById && fetchedNoteById._id) {
                return ResponseBuilder.data(fetchedNoteById, 'Note fetched successfully');
            } else {
                return ResponseBuilder.badRequest('Note not found.');
            }
        } catch (err) {
            throw ResponseBuilder.error(err);
        }
    }

    public editNote = async (id: string, updatePayload: EditNote): Promise<ResponseBuilder> => {
        try {
            const isNoteExist = await this.getById(id);
            if(isNoteExist && isNoteExist.result) {
                const updatedNote = await notes.updateOne({ _id: id}, updatePayload);
                return ResponseBuilder.data(updatedNote, 'Note edited successfully');
            } else {
                return isNoteExist;
            }
        } catch (err) {
            throw ResponseBuilder.error(err);
        }
    }

    public createNote = async (content: AddNote): Promise<ResponseBuilder> => {
        try {
            const noteInstance = await new notes(content);
            const result = await noteInstance.save();
            return ResponseBuilder.data(result, 'Note created successfully');
        } catch (err) {
            throw ResponseBuilder.error(err);
        }
    }

    public deleteNote = async (id: string): Promise<ResponseBuilder> => {
        try {
            await notes.deleteOne({ _id: id})
            return ResponseBuilder.data({}, 'Note deleted successfully');
        } catch (err) {
            throw ResponseBuilder.error(err);
        }
    }

}