import React from 'react';
import { Routes, Route } from 'react-router-dom';

import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';


import { NoteInfo } from './components/notes/note-info';
import { Notes } from './components/notes/notes';

function App() {
  return (
    <div className="App">
       <Routes>
          <Route path="/" element={<Notes />} />
          <Route path="notes/:id" element={<NoteInfo />} />
       </Routes>
       <ToastContainer />
    </div>
  );
}

export default App;
