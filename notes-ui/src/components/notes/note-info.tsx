import React, { useEffect, useState } from "react";
import { FC } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Card } from "antd";
import { noteById } from "../../services/notes.api";
import { INote, INoteForm } from "./notes.modal";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

export const NoteInfo: FC = () => {
  const params = useParams();
  const navigate = useNavigate();
  const [noteInfo, setNoteInfo] = useState<INoteForm>({
    title: "",
    content: "",
  });

  useEffect(() => {
    if (params?.id) {
      fetchNoteById(params?.id?.toString());
    } else {
      navigate("/");
    }
  }, []);

  const fetchNoteById = (id: string) => {
    noteById(id).then((res: { data: INote }) => {
      setNoteInfo({ title: res.data.title, content: res.data.content });
    });
  };

  return (
    <div style={{ height: "100vh" }}>
      <br />
      <Card
        title={noteInfo.title}
        style={{
          width: "80%",
          margin: "auto",
          height: "90vh",
          overflow: "auto",
        }}
        extra={
          <span
            onClick={() => {
              navigate("/");
            }}
          >
            <ArrowBackIcon />
          </span>
        }
      >
        <div style={{ overflowWrap: "anywhere" }}>{noteInfo.content}</div>
      </Card>
    </div>
  );
};
