export interface INote {
    "_id": string,
    "title": string,
    "content": string,
    "createdAt": string,
    "updatedAt": string,
}

export interface INoteForm {
    title: string,
    content: string,
}