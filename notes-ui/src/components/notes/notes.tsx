import React from "react";
import { FC, useEffect, useState } from "react";
import { INote, INoteForm } from "./notes.modal";
import { Card, Skeleton } from "antd";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";
import AddIcon from "@mui/icons-material/Add";
import { toast } from 'react-toastify';

import EditIcon from '@mui/icons-material/Edit';
import { Button as AntdButton, Form, Input, Empty } from 'antd';

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Fab } from "@mui/material";
import { addNote, deleteNote, getNotes, updateNote } from "../../services/notes.api";


const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
};

export const Notes: FC = () => {
    const [notes, setNotes] = useState<INote[]>([]);
    //   setting up form with initial values
    const [formValue, setFormValue] = useState<INoteForm>({ title: '', content: '' })
    const [isDeleteNoteToggleOpen, setDeleteNoteToggle] = useState({
        isOpen: false,
        id: "",
    });

    const [isAddNoteToggleOpen, setAddNoteToggle] = useState(false);

    const [isUpdateNoteToggleOpen, setUpdateNoteToggle] = useState({
        isOpen: false,
        id: "",
    });
    const navigate = useNavigate();

    const [loading, setLoading] = useState(true);

    useEffect(() => {
        fetchNotes();
    }, []);

    const fetchNotes = async () => {
        setLoading(true);
        await getNotes()
            .then((res: { data: INote[] }) => {
                setLoading(false);
                if (res && res.data) {
                    // setting up the notes
                    setNotes(res.data);
                }
            })
            .catch(() => {
                setLoading(false);
            });
    };

    const noteForm = (isAddNote = false) => {
        const { TextArea } = Input;
        return <Form
            name="note-form"
            layout="vertical"
            // labelCol={{ span: 8 }}
            onSubmitCapture={() => {
                if(formValue.title && formValue.content) {
                    if(isAddNote) {
                        addNote(formValue).then((res) => {
                            toast(res.msg);
                            setFormValue({ title: '', content: '' });
                            setAddNoteToggle(false);
                            fetchNotes();
                        })
                    } else {
                        updateNote({...formValue, id: isUpdateNoteToggleOpen.id}).then((res) => {
                            toast(res.msg);
                            setFormValue({ title: '', content: '' });
                            setUpdateNoteToggle({ isOpen: false, id: ''});
                            fetchNotes();
                        })
                    }
                }
            }}
            onValuesChange={(value) => {
                setFormValue({ ...formValue, ...value });
            }}
            initialValues={{ remember: true }}
            autoComplete="off"
        >        <br />
           <Form.Item
                label="Title"
                name="title"
                initialValue={formValue.title}
                rules={[{ required: true, message: 'Please enter title!' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item

                label="Content"
                name="content"
                initialValue={formValue.content}
                rules={[{ required: true, message: 'Please enter content!' }]}
            >
                <TextArea />
            </Form.Item>

            <Form.Item >
                <div style={{ display: 'flex'}}>
                <AntdButton type="primary" style={{marginRight: '10px'}} onClick={() => {
                   setFormValue({ title: '', content: '' });
                   setAddNoteToggle(false); 
                   setUpdateNoteToggle({ isOpen: false, id: ''});
                }}>
                    Cancel
                </AntdButton>
                <AntdButton type="primary" htmlType="submit">
                    {isAddNote ? 'Add note' : 'Update note'}
                </AntdButton>

                </div>
            </Form.Item>
        </Form>
    }

    const deleteModalContent = () => (
        <Modal
            open={isDeleteNoteToggleOpen.isOpen}
            onClose={() => {
                setDeleteNoteToggle({ id: "", isOpen: false });
            }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Are you sure?
                </Typography>
                <Typography style={{ display: "flex", marginTop: "10px" }}>
                    <Button
                        onClick={() => {
                            setDeleteNoteToggle({ id: "", isOpen: false });
                        }}
                    >
                        Cancel
                    </Button>
                    <Button
                        onClick={() => {
                            deleteNote(isDeleteNoteToggleOpen.id).then((res) => {
                                toast(res.data);
                                fetchNotes();
                                setDeleteNoteToggle({ id: "", isOpen: false });
                            });
                        }}
                    >
                        Yes, I'm Sure!
                    </Button>
                </Typography>
            </Box>
        </Modal>
    );

    const addModalContent = () => {
        return (
            <Modal
                open={isAddNoteToggleOpen}
                onClose={() => {
                    setDeleteNoteToggle({ id: "", isOpen: false });
                }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Add New Note
                    </Typography>
                    {noteForm(true)}
                </Box>
            </Modal>
        );
    };

    const concatContent = (content: string) => {
        return content.length >= 270 ? content.slice(0, 260) + '...' : content;
    }

    const editModalContent = () => {
        return (
            <Modal
                open={isUpdateNoteToggleOpen.isOpen}
                onClose={() => {
                    setDeleteNoteToggle({ id: "", isOpen: false });
                }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Update note
                    </Typography>
                    {noteForm(false)}
                </Box>
            </Modal>
        );
    };

    return (
        <React.Fragment>
            {notes.map((note, index) => (
                <Card
                    key={`note-${index}`}
                    title={
                        <span
                        style={{ textDecoration: 'underline'}}
                            onClick={() => {
                                navigate(`/notes/${note._id}`);
                            }}
                        >
                            {" "}
                            {note.title}{" "}
                        </span>
                    }
                    extra={
                        <div style={{ display: 'flex' }}>
                            <span onClick={() => {
                                setFormValue({ title: note.title, content: note.content});
                                setUpdateNoteToggle({ id: note._id, isOpen: true });
                            }}>
                                <EditIcon />
                            </span>
                            <span
                                onClick={() => {
                                    setDeleteNoteToggle({ id: note._id, isOpen: true });
                                }}
                            >
                                {" "}
                                <DeleteIcon />{" "}
                            </span>
                        </div>
                    }
                    style={{ margin: "40px", cursor: "pointer", maxHeight: "200px" }}
                >
                    <div>{note.content ? concatContent(note.content): ""}</div>
                </Card>
            ))}
            {loading ? (
                <>
                    {[1, 2, 3, 4, 5, 6, 7].map((val, index) => (
                        <Card
                            key={`note-${index}-skeleton`}
                            title={""}
                            style={{ margin: "40px" }}
                        >
                            <Skeleton
                                key={`note-${"skeleton"}`}
                                loading={loading}
                                avatar
                                active
                            >
                                <p></p>
                            </Skeleton>
                        </Card>
                    ))}
                </>
            ) : (
                ""
            )}
            {/* fab for add note */}
            <Fab size="small" color="primary" aria-label="add" onClick={() => {
                setAddNoteToggle(true)
            }}>
                <AddIcon />
            </Fab>
            {!(notes && notes.length) ? <Empty style={{marginTop: '10px'}}/> : <></>}
            {/* Starting modal content */}
            {addModalContent()}
            {editModalContent()}` {/* delete modal content */}
            {deleteModalContent()}
        </React.Fragment>
    );
};
