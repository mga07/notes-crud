import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';



const getNotes = async () => {
    const config = {
        method: 'get',
        url: `${process.env.REACT_APP_API_URL}/notes`,
        headers: {}
    };

    return await axios.request(config)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
           throw error
        });
}


const addNote = async (addNotePayload: { title: string, content: string }) => {

    const config = {
        method: 'post',
        url: `${process.env.REACT_APP_API_URL}/notes`,
        data: addNotePayload
    };

    return await axios.request(config)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
        
            throw error;

        });

}


const updateNote = async (noteContent: { title: string, content: string, id: string }) => {
    const data = JSON.stringify({
        "title": noteContent.title,
        "content": noteContent.content
    });

    const config = {
        method: 'put',
        maxBodyLength: Infinity,
        url: `${process.env.REACT_APP_API_URL}/notes/${noteContent.id}`,
        headers: {
            'Content-Type': 'application/json'
        },
        data
    };

    return await axios.request(config)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            throw error;
        });

}


const deleteNote = async (id: string) => {
    const config = {
        method: 'delete',
        maxBodyLength: Infinity,
        url: `${process.env.REACT_APP_API_URL}/notes/${id}`,
        headers: {}
    };

    return await axios.request(config)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            throw error;
        });

}


const noteById = async (id: string) => {
    const config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: `${process.env.REACT_APP_API_URL}/notes/${id}`,
    };

    return await axios.request(config)
        .then((response) => {
            return response.data
        })
        .catch((error) => {
            throw error
        });
}



export {
    getNotes, addNote,
    updateNote,
    deleteNote,
    noteById,
}